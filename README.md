# piperCI + argoCD + k3OS demo

## HowTo
1. You will need access to a k3os openstack image. Reference this repo for building: https://gitlab.com/ragingpastry/k3os-packer

2. Source your openrc file for Openstack

3. Modify image ID and network ID in the deploy.yml playbook. The image

then

`ansible-playbook deploy.yml`


What this spins up:

* K3OS instance
* ArgoCD deployed in the `argocd` namespace
* PiperCI installed as an ArgoCD project in the piperci namespace
